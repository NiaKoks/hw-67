import React, {Component} from 'react';
import "./Keyboard.css"
class Keyboard extends Component {
    state ={
        btns:['7','8','9','4','5','6','1','2','3','0'],
    }

    render() {
        return (
            <div className="keyboard">
                {  this.state.btns.map(btnId => {
                    return (<button key={btnId} className="calc-btn" onClick={()=> this.props.enterNum(btnId)}>{btnId}</button>)
                })}
                <button className="calc-btn" onClick={this.props.checkCode}>Enter</button>
                <button className="calc-btn" onClick={this.props.clearNum}>Del</button>
            </div>
        );
    }
}

export default Keyboard;