import React, {Component} from 'react';
import {connect} from "react-redux";
import Keyboard from "../Components/Keyboard/Keyboard";
import "./Calc.css";
class Calc extends Component {

    render() {

        return (
            <div className="calculator">
                <input disabled value={this.props.stars} className={"calc-inp ".concat(this.props.isRight)}/>
                <Keyboard enterNum={this.props.enterNum} checkCode={this.props.checkCode} clearNum={this.props.clearNum}/>
            </div>
        );
    }
}
const mapStateToProps = state =>{
    return{
        stars: state.stars,
        isRight: state.isRight
    }
}
const mapDispatchToProps = dispatch =>{
    return{
        enterNum: (value) => dispatch ({type:'ENTER', value}),
        checkCode: () => dispatch ({type:'CHECK'}),
        clearNum: () => dispatch ({type:'CLEAR'}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Calc);