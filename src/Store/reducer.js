const initState = {
    code:'',
    stars: '',
    isRight: '',
    trueCombination: '1111'
};

const reducer = (state = initState,action) =>{
    switch (action.type) {
        case 'ENTER':
            console.log(action.value);
            return{
                ...state,
                code:state.code + action.value,
                stars: state.stars + '*',
            };
        case 'CHECK':
            if (state.code === state.trueCombination && state.code.length === state.trueCombination.length){
                return{
                    ...state,
                    isRight: 'success'
                }
            };
            return {...state, isRight: 'danger'};
        case 'CLEAR':
            const newCode = state.code.substr(0,state.code.length - 1);
            const newStarCode = state.stars.substr(0,state.stars.length - 1);

            return{
                ...state,
                code: newCode,
                stars: newStarCode
            };

        default:
            return state
    }
}
export default reducer;